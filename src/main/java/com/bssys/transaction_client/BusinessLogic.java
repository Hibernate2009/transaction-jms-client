package com.bssys.transaction_client;

import org.apache.log4j.Logger;

public class BusinessLogic {
	private Logger log = Logger.getLogger(BusinessLogic.class);
	
	public void business(String message){
		log.info(message);
		if ("exception".equals(message)){
			log.info("Rollback " + message);
			throw new IllegalArgumentException("This is forced");
		}
	}
}